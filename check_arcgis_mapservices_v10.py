#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: expandtab ts=2 sw=2

__author__ = 'Vincent Fricou'
__copyright__ = '2020, Vincent Fricou'
__credits__ = ['Vincent Fricou']
__license__ = 'GPL'
__version__ = '0.1'
__maintainer__ = ['Vincent Fricou']
__script_name__ = 'check_arcgis_mapservices_v10.py'

import argparse
import sys
import requests
import json
import re

## Global variables

exit_hashmap = {
  "ok":       0,
  "warning":  1,
  "critical": 2,
  "unknown":  3,
}


countwarning = 0
countcritical = 0
output = ""
outputlines = 0
perf = ""


## Global Functions
def print_output(output,outputlines,countwarning,countcritical):
  if countcritical > 0:
    if outputlines > 1:
      print("Critical - Click for details\n", output)
      sys.exit(exit_hashmap["critical"])
    else:
      print("Critical -", output)
      sys.exit(exit_hashmap["critical"])
  elif countwarning > 0:
    if outputlines > 1:
      print("Warning - Click for details\n", output)
      sys.exit(exit_hashmap["warning"])
    else:
      print("Warning -", output)
      sys.exit(exit_hashmap["warning"])
  else:
    print(output)
    sys.exit(exit_hashmap["ok"])


## Execution Functions
def arcgisGenerateToken(url,username,password,referer):
  ansGenToken = requests.post(
    url,
    data={
        'username': username,
        'password': password,
        'referer': referer
    }
  )
  if ansGenToken.status_code != requests.codes.ok:
    print("Error - request failed with code {} {}",format(ansGenToken.status_code, ansGenToken.reason))
    sys.exit(exit_hashmap["critical"])
  else:
    jsonData=json.loads(ansGenToken.content)
    return jsonData['token']


def query_mapservice(hostaddress, username, password, referer, mapservice, svcdesc, warning, critical):
  if username:
    urlGenToken = "https://{}/portal/sharing/rest/generateToken?f=pjson".format(
        hostaddress)
    arcgisToken = arcgisGenerateToken(
        urlGenToken,
        username,
        password,
        referer
    )
    url = "https://{}/server/rest/services/{}/MapServer?f=pjson&token={}".format(
        hostaddress,
        mapservice,
        arcgisToken
    )
  else:
    url = "https://{}/server/rest/services/{}/MapServer?f=pjson".format(
        hostaddress,
        mapservice
    )

  svcdesc = re.sub(r"\'", " ", svcdesc, 1)
  c_warning = 0
  c_critical = 0
  ansSecRequest = requests.get(url)

  if ansSecRequest.elapsed.total_seconds() > critical:
    c_critical += 1
  elif ansSecRequest.elapsed.total_seconds() > warning:
    c_warning += 1
  perf = "request_time={};{};{}".format(
      ansSecRequest.elapsed.total_seconds(), warning, critical)
  if ansSecRequest.status_code != requests.codes.ok:
    output = "Error - request failed with code {} {} - Request time {}s".format(
        ansSecRequest.status_code,
        ansSecRequest.reason,
        ansSecRequest.elapsed.total_seconds()
    )
    c_critical += 1
    return output, perf, c_warning, c_critical
  else:
    jsonData = json.loads(ansSecRequest.content)
    jsonData["serviceDescription"] = re.sub(r"\'", " ", jsonData["serviceDescription"], 1)
    if jsonData["serviceDescription"] == svcdesc:
      output = "Expected service description - Request time {}s".format(
          ansSecRequest.elapsed.total_seconds()
      )
      return output, perf, c_warning, c_critical
    else:
      output = "Not expected service description {} - Request time {}s".format(
          jsonData["serviceDescription"],
          ansSecRequest.elapsed.total_seconds()
      )
      c_critical += 1
      return output, perf, c_warning, c_critical


## Main execution
if __name__ == "__main__":

  parser = argparse.ArgumentParser(
      description="""
            Script to check ArcGis server v10 mapservice content
        """,
      usage="",
      epilog="version {}, copyright {}".format(__version__, __copyright__))
  parser.add_argument(
      '-H', '--hostaddress',
      type=str, help='define host target',
      required=True
  )
  parser.add_argument(
      '-t', '--type',
      type=str, help='define nagios type (host, service, application)',
      required=True, choices=['secure', 'open', 'cache']
  )
  parser.add_argument(
      '-U', '--username',
      type=str, help='define arcgis server username to connect'
  )
  parser.add_argument(
      '-P', '--password',
      type=str, help='define arcgis server username to connect'
  )
  parser.add_argument(
      '-r', '--referer',
      type=str, help='define nagios type (host, service, application)',
      required=True
  )
  parser.add_argument(
    '-m', '--mapservice',
    type=str, help='define mapservice name to check',
    required=True
  )
  parser.add_argument(
    '-s', '--servicedesc',
    type=str, help='define expected mapservice description',
    required=True
  )
  parser.add_argument(
      '-w', '--warning',
      type=float, help='set warning threshold',
      default=0
  )
  parser.add_argument(
      '-c', '--critical',
      type=float, help='set critical threshold',
      default=0
  )
  parser.add_argument(
    '-p', '--perfdata',
    action='store_true', help="Set perf data parsing"
  )
  args = parser.parse_args()


  if args.type == "secure":
    output, perf, countwarning, countcritical = query_mapservice(
      args.hostaddress,
      args.username,
      args.password,
      args.referer,
      args.mapservice,
      args.servicedesc,
      args.warning,
      args.critical,
    )
  elif args.type == "open":
    output, perf, countwarning, countcritical = query_mapservice(
        args.hostaddress,
        None,
        None,
        args.referer,
        args.mapservice,
        args.servicedesc,
        args.warning,
        args.critical,
    )

  if len(perf) > 0 and args.perfdata:
    output = "{}|{}".format(output, perf)


  outputlines = output.count(',')
  output = output.replace(',', "\n")
  print_output(output,outputlines,countwarning,countcritical)
