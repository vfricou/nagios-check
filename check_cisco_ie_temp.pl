#!/usr/bin/perl -w
######################### check_snmp_name.pl #################
my $Version='1.0';
my $check_name='check_cisco_ie_temp.pl';
# Date : Apr 10 2019
# Author  : Vincent FRICOU (vincent at fricouv dot eu)
# SNMP check Cisco IE Temp sensor
################################################################

use POSIX qw(locale_h);
use strict;
use Net::SNMP;
use Getopt::Long;
use Switch;
use Number::Format qw(:subs);
use Date::Parse;
#use Data::Dumper;

### Global vars declaration

my ($o_host,$o_community,$o_port,$o_help,$o_timeout,$o_warn,$o_crit,$o_type,$o_perf,$o_expect,$o_subtype,$o_iface);
my %reverse_exit_code = (
	'Ok'=>0,
	'Warning'=>1,
	'Critical'=>2,
	'Unknown'=>3,
);

my $outputlines;
my $output          =   '';
my $perf            =   '';
my $countcritical   =   0;
my $countwarning    =   0;
my $countunknown    =   0;
my ($snmpsession,$snmperror);

### Function declaration

sub usage {
   print "\nSNMP '.$check_name.' for Nagios. Version ",$Version,"\n";
   print "GPL Licence - Vincent FRICOU\n\n";
   print <<EOT;
-h, --help
   print this help message
-H, --hostname=HOST
   name or IP address of host to check
-C, --community=COMMUNITY NAME
   community name for the host's SNMP agent (implies v2 protocol)
-f, --perfparse
   perfparse output
-w, --warning=<value>
   Warning value
-c, --critical=<value>
   Critical value
EOT
   exit $reverse_exit_code{Unknown};
}

sub get_options () {
	Getopt::Long::Configure ("bundling");
	GetOptions(
		'h|help'			=>	\$o_help,
		'H|hostname:s'	    =>	\$o_host,
		'C|community:s'	    =>	\$o_community,
		'f|perfparse'		=>	\$o_perf,
		'w|warning:s'		=>	\$o_warn,
		'c|critical:s'	    =>	\$o_crit,
	);

	usage() if (defined ($o_help));
	usage() if (! defined ($o_host) && ! defined ($o_community));
}

sub output_display () {
	$output =~ tr/,/\n/;
	if ($countcritical > 0){
		if ( $outputlines > 1 ) {
			print "Critical : Click for detail\n\n$output";
		} else {
			print "Critical - ".$output;
		}
		exit $reverse_exit_code{Critical};
	} elsif ($countwarning > 0){
		if ( $outputlines > 1 ) {
			print "Warning : Click for detail\n\n$output";
		} else {
			print "Warning - ".$output;
		}
		exit $reverse_exit_code{Warning};
	} else {
		print $output;
		exit $reverse_exit_code{Ok};
	}
}

sub get_perf_data () {
	$output=$output.'|'.$perf;
}

sub init_snmp_session () {
	my %snmpparms = (
		"-hostname"		=>	$o_host,
		"-version"		=>	2,
		"-community"	=>	$o_community,
	);
	($snmpsession,$snmperror) = Net::SNMP->session(%snmpparms);
	if (!defined $snmpsession) {
		printf "SNMP: %s\n", $snmperror;
		exit $reverse_exit_code{Unknown};
	}
}

sub check_alert_value {
    my $warning=shift;
    my $critical=shift;
	if (! defined ($warning)) { print "Warning value missing, please use -w option.\n"; exit $reverse_exit_code{Unknown}; }
	if (! defined ($critical)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}


### Main
get_options();

init_snmp_session ();
check_alert_value ($o_warn,$o_crit);
my $OIDTempNames		= '.1.3.6.1.4.1.9.9.13.1.3.1.2';
my $OIDTempValues 	= '.1.3.6.1.4.1.9.9.13.1.3.1.3';

 my $rTempNames		=   $snmpsession->get_table(-baseoid => $OIDTempNames);

if ( ! defined ($rTempNames) ) {
	$output = $output.'No Temp sensors found,';
} else {
	my $rTempValue    =   $snmpsession->get_entries(-columns => [$OIDTempValues]);
	my @tempIdx;
	my $i      			=   0;
	my $tempName;
	foreach my $idx (keys %$rTempValue) {
			my @tId = split /$OIDTempValues./, $idx;
			$tempIdx[$i] = $tId[1];
			++$i;
	}
	foreach my $tid (@tempIdx) {
			if ( $$rTempValue{$OIDTempValues.'.'.$tid} >= $o_warn ){
				if ( $$rTempValue{$OIDTempValues.'.'.$tid} >= $o_crit ) {
					++$countcritical;
				} else {
					++$countwarning;
				}
			}
			$tempName = $$rTempNames{$OIDTempNames.'.'.$tid};
			$tempName =~ s/,//g;
			$tempName =~ s/#/-/g;

			$output = $output.$tempName.$$rTempValue{$OIDTempValues.'.'.$tid}.'°C ('.$o_warn.'°C),' if ($countwarning == 0 && $countcritical == 0);
			$output = $output.$tempName.$$rTempValue{$OIDTempValues.'.'.$tid}.'°C ('.$o_crit.'°C),' if ($countwarning > 0 || $countcritical > 0);
			$perf = $perf.$tempName.'='.$$rTempValue{$OIDTempValues.'.'.$tid}.';'.$o_warn.';'.$o_crit."\n";
	}
}

$snmpsession->close;
$output =~ tr/,/\n/;
$perf =~ tr/' '/'_'/;
$outputlines = $output =~ tr/\n//;
get_perf_data() if (defined $o_perf && $o_perf ne '');
output_display();