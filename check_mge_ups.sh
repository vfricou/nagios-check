#!/bin/bash
COUNTWARNING=0
COUNTCRITICAL=0
PERF=""
export LANG="fr_FR.UTF-8"

usage() {
echo "Usage :check_mge_ups.sh
        -v SNMP version
        -C Community
        -H Host Address
        -w warning value (applicable on batrem and inttemp)
        -c critical value
        -T test to perform :
            batrem : Battery remaining time (critical must be least than warning - in mn)
            batch : Battery charge remaining time
            upsrate : UPS rated output in VA
            inttemp : UPS internal temperature (critical must be higher than warning - in °C)
            batfault : Battery fault indicator, and battery replacement status
            inphase : Input phase voltage
            infault : Input fault indicator
            outstate : Is output on battery or bypass or normal
            outload : Is overloaded output
            outtemp : Is overtemperature output."
exit 3
}

limitvalue() {
    if [ -z ${WARNING} ]; then echo "Missing warning value" ; usage ; fi
    if [ -z ${CRITICAL} ]; then echo "Missing critical value" ; usage ; fi
}

if [ "${4}" = "" ]; then usage; fi

ARGS="`echo $@ |sed -e 's:-[a-Z] :\n&:g' | sed -e 's: ::g'`"

for i in $ARGS; do
	if [ -n "`echo ${i} | grep "^\-v"`" ]; then SNMPVERSION="`echo ${i} | cut -c 3-`"; if [ ! -n ${SNMPVERSION} ]; then usage;fi;fi
	if [ -n "`echo ${i} | grep "^\-C"`" ]; then COMMUNITY="`echo ${i} | cut -c 3-`"; if [ ! -n ${COMMUNITY} ]; then usage;fi;fi
	if [ -n "`echo ${i} | grep "^\-H"`" ]; then HOSTTARGET="`echo ${i} | cut -c 3-`"; if [ ! -n ${HOSTTARGET} ]; then usage;fi;fi
	if [ -n "`echo ${i} | grep "^\-w"`" ]; then WARNING="`echo ${i} | cut -c 3-`"; if [ ! -n ${WARNING} ]; then usage;fi;fi
	if [ -n "`echo ${i} | grep "^\-c"`" ]; then CRITICAL="`echo ${i} | cut -c 3-`"; if [ ! -n ${CRITICAL} ]; then usage;fi;fi
	if [ -n "`echo ${i} | grep "^\-T"`" ]; then PERFORMTEST="`echo ${i} | cut -c 3-`"; if [ ! -n ${PERFORMTEST} ]; then usage;fi;fi
done

case ${PERFORMTEST} in
	batrem)
		limitvalue
		OIDBatRemainingTime=".1.3.6.1.4.1.705.1.4.3.0"
		OIDBatLevel=".1.3.6.1.4.1.705.1.5.2.0"
		BatRemainingTimeSec="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatRemainingTime} -On | cut -d' ' -f4-)"
		BatLevel="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatLevel} -On | cut -d' ' -f4-)"
		BatRemainingTimeMin="$(expr ${BatRemainingTimeSec} / 60)"
		if [ ${BatRemainingTimeMin} -gt ${WARNING} ]
		then
			OUTPUT="${OUTPUT}Battery remaining time : ${BatRemainingTimeMin}min (${BatLevel}%),"
		elif [ ${BatRemainingTimeMin} -le ${WARNING} ]
		then
			if [ ${BatRemainingTimeMin} -le ${CRITICAL} ]
			then
				COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
				OUTPUT="${OUTPUT}Battery remaining time : ${BatRemainingTimeMin}min (${BatLevel}%),"
			else
				COUNTWARNING=$(expr ${COUNTWARNING} + 1)
				OUTPUT="${OUTPUT}Battery remaining time : ${BatRemainingTimeMin}min (${BatLevel}%),"
			fi
		fi
		PERF="battery_remaining=${BatRemainingTimeMin};${WARNING};${CRITICAL},battery_level=${BatLevel};${WARNING};${CRITICAL}"
	;;
	batch)
		OIDBatChargeTime=".1.3.6.1.4.1.705.1.4.4.0"
		BatChargeTimeSec="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatChargeTime} -On | cut -d' ' -f4-)"
		BatChargeTimeMin="$(expr ${BatChargeTimeSec} / 60)"
		OUTPUT="${OUTPUT}Battery charging time : ${BatChargeTimeMin}min,"
		PERF="battery_charge_time=${BatChargeTimeMin}"

	;;
	upsrate)
		OIDUPSRateOutput=".1.3.6.1.4.1.705.1.4.12.0"
		UPSRateOutput="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDUPSRateOutput} -On | cut -d' ' -f4-)"
		OUTPUT="${OUTPUT}UPS Output rate : ${UPSRateOutput} VA,"
		PERF="ups_output_rate=${UPSRateOutput}"
	;;
	inttemp)
		limitvalue
		OIDUPSInTemp=".1.3.6.1.4.1.705.1.5.7.0"
		UPSInTemp="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDUPSInTemp} -On | cut -d' ' -f4-)"
		if [ ${UPSInTemp} -lt ${WARNING} ]
		then
			OUTPUT="${OUTPUT}Normal temperature : ${UPSInTemp}°C"
		elif [ ${UPSInTemp} -ge ${WARNING} ]
		then
				if [ ${UPSInTemp} -ge ${CRITICAL} ]
				then
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Temperature too high : ${UPSInTemp}°C"
				else
					COUNTWARNING=$(expr ${COUNTWARNING} + 1)
					OUTPUT="${OUTPUT}High temperature : ${UPSInTemp}°C"
				fi
		fi
		PERF="internal_temperature=${UPSInTemp};${WARNING};${CRITICAL}"
	;;
	batfault)
		OIDBatFaultIndicator=".1.3.6.1.4.1.705.1.5.9.0"
		OIDBatReplacementIndicator=".1.3.6.1.4.1.705.1.5.10.0"
		OIDBatUnavailable=".1.3.6.1.4.1.705.1.5.12.0"
		BatFaultState="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatFaultIndicator} -On | cut -d' ' -f4)"
		BatReplacementState="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatReplacementIndicator} -On | cut -d' ' -f4)"
		BatAvailabilityState="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDBatUnavailable} -On | cut -d' ' -f4)"
		if [ ${BatFaultState} -eq 2 ];
		then
			OUTPUT="${OUTPUT}Battery in normal state,"
		else
			COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
			OUTPUT="${OUTPUT}Battery faulted,"
		fi
		if [ ${BatReplacementState} -eq 2 ];
		then
			OUTPUT="${OUTPUT}Battery is operationnal,"
		else
			COUNTWARNING=$(expr ${COUNTWARNING} + 1)
			OUTPUT="${OUTPUT}Battery need to be changed,"
		fi
		if [ ${BatAvailabilityState} -ne 2 ]
		then
			COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
			OUTPUT="${OUTPUT}Battery not available,"
		fi
	;;
	inphase)
		limitvalue
		OIDPhaseNumber=".1.3.6.1.4.1.705.1.6.1.0"
		OIDPhaseVoltageIndx=".1.3.6.1.4.1.705.1.6.2.1.6"
		PhaseNumber="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDPhaseNumber} -On | cut -d' ' -f4-)"
		for phase in $(seq 1 ${PhaseNumber})
		do
			CurrentPhase="${OIDPhaseVoltageIndx}.${phase}"
			PhaseVoltage=$(snmpwalk -v${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${CurrentPhase} -On | cut -d' ' -f4-)
			if [ ${PhaseVoltage} -le $(echo ${WARNING} | cut -d',' -f1) ]
			then
				if [ ${PhaseVoltage} -le $(echo ${CRITICAL} | cut -d',' -f1) ]
				then
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Critical on phase ${phase} : ${PhaseVoltage} V,"
				else
					COUNTWARNING=$(expr ${COUNTWARNING} + 1)
					OUTPUT="${OUTPUT}Warning on phase ${phase} : ${PhaseVoltage} V,"
				fi
			fi
			if [ ${PhaseVoltage} -ge $(echo ${WARNING} | cut -d',' -f2) ]
			then
				if [ ${PhaseVoltage} -ge $(echo ${CRITICAL} | cut -d',' -f2) ]
				then
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Critical on phase ${phase} : ${PhaseVoltage} V,"
				else
					COUNTWARNING=$(expr ${COUNTWARNING} + 1)
					OUTPUT="${OUTPUT}Warning on phase ${phase} : ${PhaseVoltage} V,"
				fi
			fi
			if [ ${PhaseVoltage} -gt $(echo ${WARNING} | cut -d',' -f1) ] && [ ${PhaseVoltage} -lt $(echo ${WARNING} | cut -d',' -f2) ]
			then
				OUTPUT="${OUTPUT}Voltage on phase ${phase} : ${PhaseVoltage} V,"
			fi
			PERF="${PERF}phase_voltage_${phase}=${PhaseVoltage};$(echo ${WARNING}|cut -d',' -f2);$(echo ${CRITICAL}|cut -d',' -f2),"
		done
	;;
	infault)
		OIDInBadVoltage=".1.3.6.1.4.1.705.1.6.3.0"
		OIDInLineFailCause=".1.3.6.1.4.1.705.1.6.4.0"
		InBadVoltage="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDInBadVoltage} -On | cut -d' ' -f4)"
		InLineFailCause="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDInLineFailCause} -On | cut -d' ' -f4)"
		if [ ${InBadVoltage} -eq 2 ] && [ ${InLineFailCause} -eq 1 ]
		then
			OUTPUT="${OUTPUT}Correct voltage in input"
		else
			COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
			OUTPUT="${OUTPUT}Input failed,"
			case ${InLineFailCause} in
				1)
					OUTPUT="${OUTPUT}No Outage,"
				;;
				2)
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Voltage out of tolerance,"
				;;
				3)
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Line frequency out of tolerance,"
				;;
				4)
					COUNTCROTOCAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}No voltage,"
				;;
				*)
					COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
					OUTPUT="${OUTPUT}Case not referenced,"
				;;
			esac
		fi
	;;
	outstate)
		OIDOutBattery=".1.3.6.1.4.1.705.1.7.3.0"
		OIDOutBypass=".1.3.6.1.4.1.705.1.7.4.0"
		OutBattery="$(snmpwalk -v${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDOutBattery} -On | cut -d' ' -f4)"
		OutBypass="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDOutBypass} -On | cut -d' ' -f4)"
		if [ ${OutBattery} -eq 2 ] && [ ${OutBypass} -eq 2 ]
		then
			OUTPUT="${OUTPUT}Normal output,"
		else
			if [ ${OutBattery} -eq 1 ]
			then
				COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
				OUTPUT="${OUTPUT}Output work on battery,"
			fi
			if [ ${OutBypass} -eq 1 ]
			then
				COUNTWARNING=$(expr ${COUNTWARNING} + 1)
				OUTPUT="${OUTPUT}Output work on Bypass"
			fi
		fi
	;;
	outload)
		OIDOutloadBat=".1.3.6.1.4.1.705.1.7.10.0"
		OutloadBatState="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDOutloadBat} -On | cut -d' ' -f4)"
		if [ ${OutloadBatState} -eq 2 ]
		then
			OUTPUT="${OUTPUT}No overload on output"
		else
			COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
			OUTPUT="${OUTPUT}Overload on output"
		fi
	;;
	outtemp)
		OIDOutTemp=".1.3.6.1.4.1.705.1.7.11.0"
		OutTempState="$(snmpwalk -v ${SNMPVERSION} -c ${COMMUNITY} ${HOSTTARGET} ${OIDOutTemp} -On | cut -d' ' -f4)"
		if [ ${OutTempState} -eq 2 ]
		then
			OUTPUT="${OUTPUT}No overtemp on output"
		else
			COUNTCRITICAL=$(expr ${COUNTCRITICAL} + 1)
			OUTPUT="${OUTPUT}Overtemp on output"
		fi
;;
	*)
		usage
	;;
esac


if [ `echo $OUTPUT | tr ',' '\n' | wc -l` -gt 2 ] ;then 
	if [ $COUNTCRITICAL -gt 0 ] && [ $COUNTWARNING -gt 0 ]; then 
		echo "CRITICAL: Click for detail, "	
	else
		if [ $COUNTCRITICAL -gt 0 ]; then echo "CRITICAL: Click for detail, " ; fi
		if [ $COUNTWARNING -gt 0 ]; then echo "WARNING: Click for detail, "; fi
	fi
if [ ! $COUNTCRITICAL -gt 0 ] && [ ! $COUNTWARNING -gt 0 ]; then echo "OK: Click for detail, "; fi
	OUTPUT="`echo $OUTPUT | tr ',' '\n'`"
fi

if [ -n "$PERF" ]; then
	OUTPUT="$OUTPUT | $PERF"
fi
echo -n "$OUTPUT" | tr ',' '\n'

if [ $COUNTCRITICAL -gt 0 ]; then exit 2 ; fi
if [ $COUNTWARNING -gt 0 ]; then exit 1 ; fi
exit 0
