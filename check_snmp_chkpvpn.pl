#!/usr/bin/perl -w 
######################### check_snmp_chkpfw.pl #################
my $Version='1.0';
# Date : Jun 21 2017
# Author  : Vincent FRICOU (vincent at fricouv dot eu)
# Check for checkpoint Gateway and SMS.
# Designed for R80.10, tested and fully functionnal from R77.30.
################################################################

use POSIX qw(locale_h);
use strict;
use Net::SNMP;
use Getopt::Long;
use Switch;
use Number::Format qw(:subs);
use Date::Parse;
use Data::Dumper;

my %VPNOID = (
	'VPNInternalIP'				=> '.1.3.6.1.4.1.2620.500.9000.1.1',
	'VPNUserList'				=> '.1.3.6.1.4.1.2620.500.9000.1.2',
	'VPNType'				=> '.1.3.6.1.4.1.2620.500.9000.1.26',
	'VPNExternalIP'				=> '.1.3.6.1.4.1.2620.500.9000.1.19',
);
my ($o_host,$o_community,$o_port,$o_help,$o_timeout,$o_warn,$o_crit,$o_type,$o_perf,$o_expect,$o_subtype,$o_iface);
my %reverse_exit_code = (
	'Ok'=>0,
	'Warning'=>1,
	'Critical'=>2,
	'Unknown'=>3,
);
my $output='';
my $perf='';
my $outputlines;
my $countcritical=0;
my $countwarning=0;
my ($snmpsession,$snmperror);

sub usage {
   print "\nSNMP Checkpoint Firewalls for Nagios. Version ",$Version,"\n";
   print "GPL Licence - Vincent FRICOU\n\n";
   print <<EOT;
-h, --help
   print this help message
-H, --hostname=HOST
   name or IP address of host to check
-C, --community=COMMUNITY NAME
   community name for the host's SNMP agent (implies v1 protocol)
-f, --perfparse
   perfparse output (only works with -c)
-w, --warning=<value>
   Warning value
-c, --critical=<value>
   Critical value
-e, --expect=EXPECTED VALUE
   Specify expected value
-i, --iface=STRING
-T, --type=CHECK TYPE
EOT
   type_help();
   exit $reverse_exit_code{Unknown};
}

sub type_help {
print "If you no select subtype in case of type who need, you’ll display global status of type.
(First list level matched by -T, second level matched by -s and third level matched by -e)
  - firewall        : Check firewall state policies install state. You could provide -e value to name of policy expected on gateway
    * logging       : Check if Check Point Gateway correctly contact his logging server.
    * logstate      : Check if Check Point Gateway log in local, remote or local on error. You must specify -e value with one of following value :
                    -> remote : If you expect that the gateway log on remote server.
                    -> local  : If you expect that the gateway log locally.
    * connections   : Check active connections throught gateway. You must provide alert values.
    * pkts-ratio
      - accepted    : Display ratio accepted on total packets. (Could provide alert values)
      - dropped     : Display ratio dropped on total packets. (Could provide alert values)
      - rejected    : Display ratio rejected on total packets. (Could provide alert values)
      - logged      : Display ratio logged on total packets. (Could provide alert values)
    * bytes-ratio
      - accepted    : Display ratio accepted on total bytes. (Could provide alert values)
      - dropped     : Display ratio dropped on total bytes. (Could provide alert values)
      - rejected    : Display ratio rejected on total bytes. (Could provide alert values)
    * iface
      - accepted    : Display accepted packets ratio on specified interface.
      - dropped     : Display dropped packets ratio on specified interaface.
      - rejected    : Display rejected packets ratio on specified interface.
      - logged      : Display logged packets ratio on specified interface.
  - ha              : Check HA status
    * problems      : Display detailled units HA status.
  - temperature     : Check temperature sensors. You must specify alert values same as <CPU>,<Intake>,<Outlet> for twices
  - fan             : Check fan unit status. Perf data send fan speed. Go to critical if fan status have only one running. Warning from one faulted.
  - psu             : Check power supply units. Go to critical if power supply status have only one running. Warning from one faulted.
  - svn             : Check SVN status.
  - mgmt            : Check management status
    * licence       : Check licence violation.
    * active        : Check active and alive management blade.
  - anti-virus      : Check Anti-Virus status
    * subscription  : Check subscription status and expiration date.
    * updates       : Check Application Control update status.
  - anti-spam       : Check Anti-Spam status
    * stats         : Check anti-spam statistics ratio. You must provide alert values
    * detailed      : Check anti-spam detailed spam analysis.
    * subscription  : Check subscription status and expiration date.
  - identity-awareness : Check Identity Awareness global status.
    * connections
      - kerberos    : Return number of users and hosts connected throught kerberos. (Could provide alert values)
      - usrpass     : Return number of users authenticated with simple user/password scheme.
      - ad          : Return number of users and hosts authenticated throught Active Directory (Could provide alert values)
      - agent       : Return number of users authenticated with Check Point Agent.
      - portal      : Return number of users authenticated throught web portal.
      - at-login    : Return total of entities autenticated throught AD.
    * auth-server   : Check authentication servers status. (Threshold automated thought check returns).
  - application-control : Check Application Control global status.
    * subscription  : Check subscription status and expiration date.
    * updates       : Check Application Control update status.
  - urlfiltering    : Check URL Filtering global status.
    * subscription  : Check subscription status and expiration date.
    * updates       : Check URL Filtering update status.
    * radstatus     : Check RAD Status update status.
  - anti-bot        : Check Anti-Bot/Anti-Malware status
    * subscription  : Check subscription status and expiration date.
    * updates       : Check Application Control update status.
  - tunnel          : Check status of all declared VPN tunnels
";
	exit $reverse_exit_code{Unknown};
}

sub expect_help {
	print "Missing expected value.
	Please provide value by the option -e (or --expect=) report to type of check you run.\n";
	exit $reverse_exit_code{Unknown};
}
sub multi_threshold_usage {
	print "You must specify two threshold for alert values in this usecase.
	To do this, please, use -w and -c options with comma separated values.\n";
}
sub get_options () {
	Getopt::Long::Configure ("bundling");
	GetOptions(
		'h'		=>	\$o_help,		'help'			=>	\$o_help,
		'H:s'	=>	\$o_host,		'hostname:s'	=>	\$o_host,
		'C:s'	=>	\$o_community,	'community:s'	=>	\$o_community,
		'f'		=>	\$o_perf,		'perfparse'		=>	\$o_perf,
		'w:s'	=>	\$o_warn,		'warning:s'		=>	\$o_warn,
		'c:s'	=>	\$o_crit,		'critical:s'	=>	\$o_crit,
		'T:s'	=>	\$o_type,		'type:s'		=>	\$o_type,
		's:s'	=>	\$o_subtype,	'subtype:s'		=>	\$o_subtype,
		'e:s'	=>	\$o_expect,		'expect:s'		=>	\$o_expect,
		'i:s'	=>	\$o_iface,		'iface:s'		=>	\$o_iface,
	);

	usage() if (defined ($o_help));
	usage() if (! defined ($o_host) && ! defined ($o_community) && ! defined ($o_type));
	type_help() if (! defined ($o_type));
	$o_type=uc($o_type);
	$o_subtype=uc($o_subtype) if (defined $o_subtype);
	$o_expect=lc($o_expect) if (defined ($o_expect));
}

sub output_display () {
	$output =~ tr/,/\n/;
	if ($countcritical > 0){
		if ( $outputlines > 1 ) {
			print "Critical : Click for detail\n\n$output";
		} else {
			print "Critical - ".$output;
		}
		exit $reverse_exit_code{Critical};
	} elsif ($countwarning > 0){
		if ( $outputlines > 1 ) {
			print "Warning : Click for detail\n\n$output";
		} else {
			print "Warning - ".$output;
		}
		exit $reverse_exit_code{Warning};
	} else {
		print $output;
		exit $reverse_exit_code{Ok};
	}
}

sub get_perf_data () {
	$output=$output.'|'.$perf;
}

sub init_snmp_session () {
	my %snmpparms = (
		"-hostname"		=>	$o_host,
		"-version"		=>	2,
		"-community"	=>	$o_community,
	);
	($snmpsession,$snmperror) = Net::SNMP->session(%snmpparms);
	if (!defined $snmpsession) {
		printf "SNMP: %s\n", $snmperror;
		exit $reverse_exit_code{Unknown};
	}
}

sub check_alert_value () {
	if (! defined ($o_warn)) { print "Warning value missing, please use -w option.\n"; exit $reverse_exit_code{Unknown}; }
	if (! defined ($o_crit)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}

get_options();

switch ($o_type) {
	case 'VPN' {
		init_snmp_session();
		check_alert_value();
		my @VpnIPID;
		my $valcount=0;
		my $rsVPNEntries = $snmpsession->get_entries(-columns => [$VPNOID{'VPNInternalIP'}]);
		my $retBash = `snmpwalk -Ovq -On -v2c -c $o_community $o_host -Cc $VPNOID{'VPNInternalIP'}`;
		my @arrRetBash = split(/\n/, $retBash);
		#print Dumper(@arrRetBash);
		foreach my $val (@arrRetBash) {
			push(@VpnIPID,$val);
			my $rsVPNUser		= $snmpsession->get_request(-varbindlist => [$VPNOID{'VPNUserList'}.'.'.$VpnIPID[$valcount].'.0']);
			my $rsVPNType		= $snmpsession->get_request(-varbindlist => [$VPNOID{'VPNType'}.'.'.$VpnIPID[$valcount].'.0']);
			my $rsVPNExtIP		= $snmpsession->get_request(-varbindlist => [$VPNOID{'VPNExternalIP'}.'.'.$VpnIPID[$valcount].'.0']);
			$output			= $output.'User '.$$rsVPNUser{$VPNOID{'VPNUserList'}.'.'.$VpnIPID[$valcount].'.0'};
			$output			= $output.' connected from '.$$rsVPNExtIP{$VPNOID{'VPNExternalIP'}.'.'.$VpnIPID[$valcount].'.0'};
			$output			= $output.' as '.$$rsVPNType{$VPNOID{'VPNType'}.'.'.$VpnIPID[$valcount].'.0'}.',';
			++$valcount;
		}
		if ($valcount >= $o_warn) {
			if ($valcount >= $o_crit) {
				++$countcritical;
				$output = 'Critical - '.$valcount.' active connections,,'.$output;
			} else {
				++$countwarning;
				$output = 'Warning - '.$valcount.' active connections,,'.$output;
			}
		} else {
			$output = 'OK - '.$valcount.' active connections,,'.$output;
		}
		$perf = 'vpn_connections='.$valcount.';'.$o_warn.';'.$o_crit.',';			
	} else { $output = 'Type not recognized.'; type_help(); }
}
$snmpsession->close;
$output =~ tr/,/\n/;
$perf =~ tr/' '/'_'/;
$outputlines = $output =~ tr/\n//;
get_perf_data() if (defined $o_perf && $o_perf ne '');
output_display();
