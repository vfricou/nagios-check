#!/usr/bin/perl -w
######################### check_snmp_name.pl #################
my $Version='1.0.1';
my $check_name='check_snmp_cs121.pl';
# Date : Mar 25 2019
# Author  : Vincent FRICOU (vincent at fricouv dot eu)
# SNMP check for CS121 UPS
################################################################

use POSIX qw(locale_h);
use strict;
use Net::SNMP;
use Getopt::Long;
use Switch;
use Number::Format qw(:subs);
use Date::Parse;
use Data::Dumper;

### Global vars declaration

my ($o_host,$o_community,$o_port,$o_help,$o_timeout,$o_warn,$o_crit,$o_type,$o_perf,$o_expect,$o_subtype,$o_iface);
my %reverse_exit_code = (
	'Ok'=>0,
	'Warning'=>1,
	'Critical'=>2,
	'Unknown'=>3,
);

my $outputlines;
my $output          =   '';
my $perf            =   '';
my $countcritical   =   0;
my $countwarning    =   0;
my $countunknown    =   0;
my ($snmpsession,$snmperror);

### Custom hashs

my %battery_exit_code = (
    1   =>  'unknown',
    2   =>  'normal',
    3   =>  'batteryLow',
    4   =>  'batteryDepleted',
);

my %rev_battery_exit_code = (
    'unknown'           =>  1,
    'normal'            =>  2,
    'batteryLow'        =>  3,
    'batteryDepleted'   =>  4,
);

my %output_exit_code = (
    1                   =>  'other',
    2                   =>  'none',
    3                   =>  'normal',
    4                   =>  'bypass',
    5                   =>  'battery',
    6                   =>  'booster',
    7                   =>  'reducer',
);

my %rev_output_exit_code = (
    'other'             =>  1,
    'none'              =>  2,
    'normal'            =>  3,
    'bypass'            =>  4,
    'battery'           =>  5,
    'booster'           =>  6,
    'reducer'           =>  7,
);

### OID vars declaration

my %OIDBattery = (
    'Table'                 =>  '.1.3.6.1.2.1.33.1.2',
    'Status'                =>  '.1.3.6.1.2.1.33.1.2.1.0',
    'SecsOnBat'             =>  '.1.3.6.1.2.1.33.1.2.2.0',
    'MinRemain'             =>  '.1.3.6.1.2.1.33.1.2.3.0',
    'PctRemain'             =>  '.1.3.6.1.2.1.33.1.2.4.0',
    'BatVolt'               =>  '.1.3.6.1.2.1.33.1.2.5.0',
    'BatCurr'               =>  '.1.3.6.1.2.1.33.1.2.6.0',
);

my %OIDInput = (
    'Index'                 =>  '.1.3.6.1.2.1.33.1.3.3.1.1',
    'Voltage'               =>  '.1.3.6.1.2.1.33.1.3.3.1.3',
    'Current'               =>  '.1.3.6.1.2.1.33.1.3.3.1.4',
    'Power'                 =>  '.1.3.6.1.2.1.33.1.3.3.1.5',
);

my %OIDOutput = (
    'Source'                =>  '.1.3.6.1.2.1.33.1.4.1.0',
    'Table'                 =>  '.1.3.6.1.2.1.33.1.4.4.1.1',
    'Voltage'               =>  '.1.3.6.1.2.1.33.1.4.4.1.2',
    'Current'               =>  '.1.3.6.1.2.1.33.1.4.4.1.3',
    'Power'                 =>  '.1.3.6.1.2.1.33.1.4.4.1.4',
    'Load'                  =>  '.1.3.6.1.2.1.33.1.4.4.1.5',
);

my %OIDAlarms = (
    'Presence'              =>  '.1.3.6.1.2.1.33.1.6.1.0',
);

### Function declaration

sub usage {
   print "\nSNMP '.$check_name.' for Nagios. Version ",$Version,"\n";
   print "GPL Licence - Vincent FRICOU\n\n";
   print <<EOT;
-h, --help
   print this help message
-H, --hostname=HOST
   name or IP address of host to check
-C, --community=COMMUNITY NAME
   community name for the host's SNMP agent (implies v2 protocol)
-f, --perfparse
   perfparse output
-w, --warning=<value>
   Warning value
-c, --critical=<value>
   Critical value
-T, --type=CHECK TYPE
EOT
   type_help();
   exit $reverse_exit_code{Unknown};
}

sub type_help {
print "    - oper-state: Set critical alert when UPS start run on battery and stats time on battery.
    - battery-state: Check battery state and stat remaining time on battery and current voltage/current of battery
      You need set warning/critical values according to remaining time (in minuts) on battery.
    - input: Check input line status
      You need set warning/critical values according to minimal/maximal voltage accorded on line (as format min:max for each states)
    - output: Check output line status
      You need set warning/critical values according to format minvolt:maxvolt,maxload
    - alarm: Check active alarms on UPS
      Set critical if had one or more active alarm
";
	exit $reverse_exit_code{Unknown};
}
sub get_options () {
	Getopt::Long::Configure ("bundling");
	GetOptions(
		'h|help'			=>	\$o_help,
		'H|hostname:s'	    =>	\$o_host,
		'C|community:s'	    =>	\$o_community,
		'f|perfparse'		=>	\$o_perf,
		'w|warning:s'		=>	\$o_warn,
		'c|critical:s'	    =>	\$o_crit,
		'T|type:s'		    =>	\$o_type,
	);

	usage() if (defined ($o_help));
	usage() if (! defined ($o_host) && ! defined ($o_community) && ! defined ($o_type));
	type_help() if (! defined ($o_type));
	$o_type=uc($o_type);
}

sub output_display () {
	$output =~ tr/,/\n/;
	if ($countcritical > 0){
		if ( $outputlines > 1 ) {
			print "Critical : Click for detail\n\n$output";
		} else {
			print "Critical - ".$output;
		}
		exit $reverse_exit_code{Critical};
	} elsif ($countwarning > 0){
		if ( $outputlines > 1 ) {
			print "Warning : Click for detail\n\n$output";
		} else {
			print "Warning - ".$output;
		}
		exit $reverse_exit_code{Warning};
	} else {
		print $output;
		exit $reverse_exit_code{Ok};
	}
}

sub get_perf_data () {
	$output=$output.'|'.$perf;
}

sub init_snmp_session () {
	my %snmpparms = (
		"-hostname"		=>	$o_host,
		"-version"		=>	2,
		"-community"	=>	$o_community,
	);
	($snmpsession,$snmperror) = Net::SNMP->session(%snmpparms);
	if (!defined $snmpsession) {
		printf "SNMP: %s\n", $snmperror;
		exit $reverse_exit_code{Unknown};
	}
}

sub check_alert_value {
    my $warning=shift;
    my $critical=shift;
	if (! defined ($warning)) { print "Warning value missing, please use -w option.\n"; exit $reverse_exit_code{Unknown}; }
	if (! defined ($critical)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}

sub check_fork_alert_value {
    my $warning=shift;
    my $critical=shift;
	if (! defined ($warning)) {
        print "Warning value missing, please use -w option.\n";
        exit $reverse_exit_code{Unknown};
    } else {
        my @awarn = split(':', $warning);
        if (! defined ($awarn[0])) {
            print "Warning low value missing, please specify warning value as format 'low,high'.\n";
            exit $reverse_exit_code{Unknown};
        } elsif (! defined ($awarn[1])) {
            print "Warning high value missing, please specify warning value as format 'low,high'.\n";
            exit $reverse_exit_code{Unknown};
        }

        my @acrit = split(':', $critical);
        if (! defined ($acrit[0])) {
            print "Critical low value missing, please specify critical value as format 'low,high'.\n";
            exit $reverse_exit_code{Unknown};
        } elsif (! defined ($acrit[1])) {
            print "Critical high value missing, please specify critical value as format 'low,high'.\n";
            exit $reverse_exit_code{Unknown};
        }
    }
	if (! defined ($critical)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}

### Main
get_options();

switch ($o_type) {
	case 'OPER-STATE' {
        init_snmp_session();

        my $result;
        $result             =   $snmpsession->get_request(-varbindlist => [$OIDBattery{'Status'}]);
        my $BatStatus       =   $$result{$OIDBattery{'Status'}};
        $result             =   $snmpsession->get_request(-varbindlist => [$OIDBattery{'SecsOnBat'}]);
        my $SecsOnBat       =   $$result{$OIDBattery{'SecsOnBat'}};
        my $RunOnBat        =   0;

        if ( $BatStatus != $rev_battery_exit_code{'normal'} ) {
            ++$countwarning if ( $BatStatus == $rev_battery_exit_code{'batteryLow'} );
            ++$countcritical if ( $BatStatus == $rev_battery_exit_code{'batteryDepleted'} );
      		++$countunknown if ( $BatStatus == $rev_battery_exit_code{'unknown'} );
        }

        if ( $SecsOnBat != 0 ) {
            ++$countcritical;
            $output = $output.'Battery is in state '.$battery_exit_code{$BatStatus}.' and passed '.$SecsOnBat.' seconds on battery,';
        } else {
            $output = $output.'Battery is in state '.$battery_exit_code{$BatStatus}.',';
        }
        $perf = $perf.'SecsOnBattery='.$SecsOnBat.";;\n";
	}

    case 'BATTERY-STATE' {
        check_alert_value($o_warn, $o_crit);
        init_snmp_session();
        my $result;

        $result         =   $snmpsession->get_entries(-columns => [$OIDBattery{'Table'}]);

        my $BatRemain   =   $$result{$OIDBattery{'MinRemain'}};
        my $PctRemain   =   $$result{$OIDBattery{'PctRemain'}};
        my $BatVolt     =   $$result{$OIDBattery{'BatVolt'}};
        my $BatCurr     =   $$result{$OIDBattery{'BatCurr'}};

        if ( $BatRemain <= $o_warn) {
            if ( $BatRemain <= $o_crit ) {
                ++$countcritical;
            } else {
                ++$countwarning;
            }
        }

        if ( $BatCurr > 0 ) {
            $output = $output.'Battery remaining time '.$BatRemain.' minutes ('.$PctRemain.'%) with battery condition at '.$BatVolt.'V and '.$BatCurr.'A,';
        } else {
            $output = $output.'Battery remaining time '.$BatRemain.' minutes ('.$PctRemain.'%) with battery condition at '.$BatVolt.'V,';
        }

        $perf=$perf."Minutes_Remaining=".$BatRemain.';'.$o_warn.';'.$o_crit."\n";
        $perf=$perf."Pct_Remaining=".$PctRemain.";;\n";
        $perf=$perf."Battery_Voltage=".$BatVolt.";;\n";
        $perf=$perf."Battery_Current=".$BatCurr.";;\n";
    }

    case 'INPUT' {
        check_fork_alert_value($o_warn,$o_crit);
        init_snmp_session();

        my @warnvals    =   split(':', $o_warn);
        my @critvals    =   split(':', $o_crit);

        my $InIdx       =   $snmpsession->get_entries(-columns => [$OIDInput{'Index'}]);

        foreach my $Input (keys %$InIdx) {
            my $resInVolt       =   $snmpsession->get_request(-varbindlist => [$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}.'.0']);
            my $resInCurr       =   $snmpsession->get_request(-varbindlist => [$OIDInput{'Current'}.'.'.$$InIdx{$Input}.'.0']);
            my $resInPow        =   $snmpsession->get_request(-varbindlist => [$OIDInput{'Power'}.'.'.$$InIdx{$Input}.'.0']);

            if ( $$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}} <= $warnvals[0] ) {
                if ( $$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}} <= $critvals[0] ) {
                    ++$countcritical;
                } else {
                    ++$countwarning;
                }
            } elsif ( $$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}} >= $warnvals[1] ) {
                if ( $$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}} >= $critvals[1] ) {
                    ++$countcritical;
                } else {
                    ++$countwarning;
                }
            }
            if ( $$resInCurr{$OIDInput{'Current'}.'.'.$$InIdx{$Input}} > 0 ) {
                if (  $$resInPow{$OIDInput{'Power'}.'.'.$$InIdx{$Input}} > 0 ) {
                    $output = $output.'Input line '.$$InIdx{$Input}.' voltage value '.$$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}}.'V. Input current value '.$$resInCurr{$OIDInput{'Current'}.'.'.$$InIdx{$Input}}.'A. Input power value '. $$resInPow{$OIDInput{'Power'}.'.'.$$InIdx{$Input}}.',';
                } else {
                    $output = $output.'Input line '.$$InIdx{$Input}.' voltage value '.$$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}}.'V. Input current value '.$$resInCurr{$OIDInput{'Current'}.'.'.$$InIdx{$Input}}.'A,';
                }
            } else {
                if (  $$resInPow{$OIDInput{'Power'}.'.'.$$InIdx{$Input}} > 0 ) {
                    $output = $output.'Input line '.$$InIdx{$Input}.' voltage value '.$$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}}.'V. Input power value '. $$resInPow{$OIDInput{'Power'}.'.'.$$InIdx{$Input}}.',';
                } else {
                    $output = $output.'Input line '.$$InIdx{$Input}.' voltage value '.$$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}}.'V.,';
                }
            }
            $perf = $perf.'Voltage_Input_'.$$InIdx{$Input}.'='.$$resInVolt{$OIDInput{'Voltage'}.'.'.$$InIdx{$Input}}.';'.$o_warn.';'.$o_crit."\n";
            $perf = $perf.'Current_Input_'.$$InIdx{$Input}.'='.$$resInCurr{$OIDInput{'Current'}.'.'.$$InIdx{$Input}}.";;\n";
            $perf = $perf.'Power_Input_'.$$InIdx{$Input}.'='.$$resInPow{$OIDInput{'Power'}.'.'.$$InIdx{$Input}}.";;\n";
        }
    }
    case 'OUTPUT' {
        my @awarn               =   split(',', $o_warn);
        my @acrit               =   split(',', $o_crit);
        check_alert_value($awarn[1], $acrit[1]);
        check_fork_alert_value($awarn[0], $acrit[0]);
        init_snmp_session();

        my @vwarn               =   split(':', $awarn[0]);
        my @vcrit               =   split(':', $acrit[0]);

        my $resSrcStatus        =   $snmpsession->get_request(-varbindlist => [$OIDOutput{'Source'}]);

        if ( $$resSrcStatus{$OIDOutput{'Source'}} != $rev_output_exit_code{'normal'} ) {
            if ( $$resSrcStatus{$OIDOutput{'Source'}} != $rev_output_exit_code{'battery'} ) {
                ++$countwarning;
            } else {
                ++$countcritical;
            }
        }

        $output = $output.'Output source status run on '.$output_exit_code{$$resSrcStatus{$OIDOutput{'Source'}}}.',';

        my $OutIdx              =   $snmpsession->get_entries(-columns => [$OIDOutput{'Table'}]);

        foreach my $Output (keys %$OutIdx) {
            my $resOutVolt  =   $snmpsession->get_request(-varbindlist => [$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}]);
            my $resOutCurr  =   $snmpsession->get_request(-varbindlist => [$OIDOutput{'Current'}.'.'.$$OutIdx{$Output}]);
            my $resOutPow   =   $snmpsession->get_request(-varbindlist => [$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}]);
            my $resOutLoad  =   $snmpsession->get_request(-varbindlist => [$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}]);

            if ( $$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}} < $vwarn[0] ) {
                if ( $$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}} < $vcrit[0] ) {
                    ++$countcritical;
                } else {
                    ++$countwarning;
                }
            } elsif ( $$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}} >= $vwarn[1] ) {
                if ( $$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}} >= $vcrit[1] ) {
                    ++$countcritical;
                } else {
                    ++$countwarning;
                }
            }
            if ( $$resOutVolt{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}} > $awarn[1] ) {
                if ( $$resOutVolt{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}} > $acrit[1] ) {
                    ++$countcritical;
                } else {
                    ++$countwarning;
                }
            }
            if ( $$resOutCurr{$OIDOutput{'Current'}.'.'.$$OutIdx{$Output}} > 0 ) {
                if (  $$resOutPow{$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}} > 0 ) {
                    $output = $output.'Output line '.$$OutIdx{$Output}.' load '.$$resOutLoad{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}}.'. Voltage value '.$$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}}.'V. Output current value '.$$resOutCurr{$OIDOutput{'Current'}.'.'.$$OutIdx{$Output}}.'A. Output power value '. $$resOutPow{$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}}.',';
                } else {
                    $output = $output.'Output line '.$$OutIdx{$Output}.' load '.$$resOutLoad{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}}.'. Voltage value '.$$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}}.'V. Output current value '.$$resOutCurr{$OIDOutput{'Current'}.'.'.$$OutIdx{$Output}}.'A,';
                }
            } else {
                if (  $$resOutPow{$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}} > 0 ) {
                    $output = $output.'Output line '.$$OutIdx{$Output}.' load '.$$resOutLoad{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}}.'. Voltage value '.$$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}}.'V. Output power value '. $$resOutPow{$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}}.',';
                } else {
                    $output = $output.'Output line '.$$OutIdx{$Output}.' load '.$$resOutLoad{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}}.'. Voltage value '.$$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}}.'V.,';
                }
            }

            $perf = $perf.'Voltage_Output_'.$$OutIdx{$Output}.'='.$$resOutVolt{$OIDOutput{'Voltage'}.'.'.$$OutIdx{$Output}}.';'.$vwarn[0].':'.$vwarn[1].';'.$vcrit[0].':'.$vcrit[1]."\n";
            $perf = $perf.'Current_Output_'.$$OutIdx{$Output}.'='.$$resOutCurr{$OIDOutput{'Current'}.'.'.$$OutIdx{$Output}}.";;\n";
            $perf = $perf.'Power_Output_'.$$OutIdx{$Output}.'='.$$resOutPow{$OIDOutput{'Power'}.'.'.$$OutIdx{$Output}}.";;\n";
            $perf = $perf.'Load_Output_'.$$OutIdx{$Output}.'='.$$resOutLoad{$OIDOutput{'Load'}.'.'.$$OutIdx{$Output}}.';'.$awarn[1].';'.$acrit[1]."\n";
        }
    }

    case 'ALARM'{
        init_snmp_session();

        my $resAlarmPres    =   $snmpsession->get_request(-varbindlist => [$OIDAlarms{'Presence'}]);

        if ( $$resAlarmPres{$OIDAlarms{'Presence'}} > 0 ) {
            ++$countcritical;
        }
        $output             =   $output.'Alarm count on UPS '.$$resAlarmPres{$OIDAlarms{'Presence'}}.',';
        $perf               =   $perf.'Alarm_count='.$$resAlarmPres{$OIDAlarms{'Presence'}}.";;\n";

    } else { $output = 'Type not recognized.'; }
}

$snmpsession->close;
$output =~ tr/,/\n/;
$perf =~ tr/' '/'_'/;
$outputlines = $output =~ tr/\n//;
get_perf_data() if (defined $o_perf && $o_perf ne '');
output_display();