# Global bash script skeleton

```bash
#!/bin/bash

CHECKNAME="check_name.sh"
REVISION="0.1"

usage () {
echo "Usage : ${CHECKNAME} - Version : ${REVISION}
        "
exit 3
}


out() {
	if [ $COUNTCRITICAL -ge 1 ] || [ $COUNTWARNING -ge 1 ];then
		if [ $COUNTWARNING -ge 1 ];then
			echo "Warning : click for details,"
			echo -n "$OUTPUT," | tr ',' '\n'
		fi
		if [ $COUNTCRITICAL -ge 1 ] ; then
			echo "Critical : click for details,"
			echo -n "$OUTPUT," | tr ',' '\n'
		fi
	else
		echo "Ok : click for details,"
		echo -n "$OUTPUT," | tr ',' '\n'
	fi
	if [ -n "$PERF" ]; then
        	echo " | $PERF"
	fi
	if [ $COUNTCRITICAL -gt 0 ]; then exit 2 ; fi
	if [ $COUNTWARNING -gt 0 ]; then exit 1 ; fi
	exit 0
}
ARGS="$(echo $@ |sed -e 's:-[[:alpha:]] :\n&:g' | sed -e 's: ::g')"
for i in $ARGS; do
        if [ -n "`echo ${i} | grep "^\-H"`" ]; then HOSTTARGET="`echo ${i} | cut -c 3-`"; if [ ! -n ${HOSTTARGET} ]; then usage;fi;fi
        if [ -n "`echo ${i} | grep "^\-C"`" ]; then COMMUNITY="`echo ${i} | cut -c 3-`"; if [ ! -n ${COMMUNITY} ]; then usage;fi;fi
        if [ -n "`echo ${i} | grep "^\-v"`" ]; then VERSION="`echo ${i} | cut -c 3-`"; if [ ! -n ${VERSION} ]; then usage;fi;fi
        if [ -n "`echo ${i} | grep "^\-P"`" ]; then PORT="`echo ${i} | cut -c 3-`"; if [ ! -n ${PORT} ]; then usage;fi;fi
        if [ -n "`echo ${i} | grep "^\-c"`" ]; then CRITICAL="`echo ${i} | cut -c 3-`"; if [ ! -n ${CRITICAL} ]; then usage;fi;fi
        if [ -n "`echo ${i} | grep "^\-w"`" ]; then WARNING="`echo ${i} | cut -c 3-`"; if [ ! -n ${WARNING} ]; then usage;fi;fi
done
```