# Perl check Skeleton

```perl
#!/usr/bin/perl -w 
######################### check_snmp_name.pl #################
my $Version='0.1';
my $check_name='check_snmp_name.pl';
# Date : Aug 16 2017
# Author  : Vincent FRICOU (vincent at fricouv dot eu)
# Generic skeleton for perl check.
################################################################

use POSIX qw(locale_h);
use strict;
use Net::SNMP;
use Getopt::Long;
use Switch;
use Number::Format qw(:subs);
use Date::Parse;
use Data::Dumper;

### Global vars declaration

my ($o_host,$o_community,$o_port,$o_help,$o_timeout,$o_warn,$o_crit,$o_type,$o_perf,$o_expect,$o_subtype,$o_iface);
my %reverse_exit_code = (
	'Ok'=>0,
	'Warning'=>1,
	'Critical'=>2,
	'Unknown'=>3,
);

my $output='';
my $perf='';
my $outputlines;
my $countcritical=0;
my $countwarning=0;
my ($snmpsession,$snmperror);

### OID vars declaration


### Function declaration

sub usage {
   print "\nSNMP '.$check_name.' for Nagios. Version ",$Version,"\n";
   print "GPL Licence - Vincent FRICOU\n\n";
   print <<EOT;
-h, --help
   print this help message
-H, --hostname=HOST
   name or IP address of host to check
-C, --community=COMMUNITY NAME
   community name for the host's SNMP agent (implies v2 protocol)
-f, --perfparse
   perfparse output
-w, --warning=<value>
   Warning value
-c, --critical=<value>
   Critical value
-T, --type=CHECK TYPE
EOT
   type_help();
   exit $reverse_exit_code{Unknown};
}

sub type_help {
print "Here desc for check type
";
	exit $reverse_exit_code{Unknown};
}
sub get_options () {
	Getopt::Long::Configure ("bundling");
	GetOptions(
		'h'		=>	\$o_help,		'help'			=>	\$o_help,
		'H:s'	=>	\$o_host,		'hostname:s'	=>	\$o_host,
		'C:s'	=>	\$o_community,	'community:s'	=>	\$o_community,
		'f'		=>	\$o_perf,		'perfparse'		=>	\$o_perf,
		'w:s'	=>	\$o_warn,		'warning:s'		=>	\$o_warn,
		'c:s'	=>	\$o_crit,		'critical:s'	=>	\$o_crit,
		'T:s'	=>	\$o_type,		'type:s'		=>	\$o_type,
	);

	usage() if (defined ($o_help));
	usage() if (! defined ($o_host) && ! defined ($o_community) && ! defined ($o_type));
	type_help() if (! defined ($o_type));
	$o_type=uc($o_type);
}

sub output_display () {
	$output =~ tr/,/\n/;
	if ($countcritical > 0){
		if ( $outputlines > 1 ) {
			print "Critical : Click for detail\n\n$output";
		} else {
			print "Critical - ".$output;
		}
		exit $reverse_exit_code{Critical};
	} elsif ($countwarning > 0){
		if ( $outputlines > 1 ) {
			print "Warning : Click for detail\n\n$output";
		} else {
			print "Warning - ".$output;
		}
		exit $reverse_exit_code{Warning};
	} else {
		print $output;
		exit $reverse_exit_code{Ok};
	}
}

sub get_perf_data () {
	$output=$output.'|'.$perf;
}

sub init_snmp_session () {
	my %snmpparms = (
		"-hostname"		=>	$o_host,
		"-version"		=>	2,
		"-community"	=>	$o_community,
	);
	($snmpsession,$snmperror) = Net::SNMP->session(%snmpparms);
	if (!defined $snmpsession) {
		printf "SNMP: %s\n", $snmperror;
		exit $reverse_exit_code{Unknown};
	}
}

sub check_alert_value () {
	if (! defined ($o_warn)) { print "Warning value missing, please use -w option.\n"; exit $reverse_exit_code{Unknown}; }
	if (! defined ($o_crit)) { print "Critical value missing, please use -c option.\n"; exit $reverse_exit_code{Unknown}; }
}
sub check_subtype () {
	if (! defined ($o_subtype)) { print "No subtype defined, please use -s option.\n"; exit $reverse_exit_code{Unknown}; }
}


### Main
get_options();

switch ($o_type) {
	case 'GENERIC' {

	} else { $output = 'Type not recognized.'; }
}
$snmpsession->close;
$output =~ tr/,/\n/;
$perf =~ tr/' '/'_'/;
$outputlines = $output =~ tr/\n//;
get_perf_data() if (defined $o_perf && $o_perf ne '');
output_display();

```