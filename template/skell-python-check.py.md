# Python check skeleton

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: expandtab ts=2 sw=2

__author__ = 'Vincent Fricou'
__copyright__ = '2020, Vincent Fricou'
__credits__ = ['Vincent Fricou']
__license__ = 'GPL'
__version__ = '0.1'
__maintainer__ = ['Vincent Fricou']
__script_name__ = 'skll-python-check.py'

import argparse
import sys

## Global variables

exit_hashmap = {
  "ok":       0,
  "warning":  1,
  "critical": 2,
  "unknown":  3,
}


countwarning = 0
countcritical = 0
output = ""
outputlines = 0
perf = ""


## Global Functions
def print_output(output,outputlines,countwarning,countcritical):
  if countcritical > 0:
    if outputlines > 1:
      print("Critical - Click for details\n", output)
      sys.exit(exit_hashmap["critical"])
    else:
      print("Critical -", output)
      sys.exit(exit_hashmap["critical"])
  elif countwarning > 0:
    if outputlines > 1:
      print("Warning - Click for details\n", output)
      sys.exit(exit_hashmap["warning"])
    else:
      print("Warning -", output)
      sys.exit(exit_hashmap["warning"])
  else:
    print(output)
    sys.exit(exit_hashmap["ok"])


## Execution Functions
def example1():
  global output
  output = "Example 1"


def example2():
  global output
  output = "Example 2"


## Main execution
if __name__ == "__main__":

  parser = argparse.ArgumentParser(
      description="""
            Script to check control point
        """,
      usage="",
      epilog="version {}, copyright {}".format(__version__, __copyright__))
  parser.add_argument(
      '-H', '--hostaddress',
      type=str, help='define host target',
      required=True
  )
  parser.add_argument(
      '-t', '--type',
      type=str, help='define nagios type (host, service, application)',
      required=True, choices=['example1', 'example2']
  )
  parser.add_argument(
      '-w', '--warning',
      type=int, help='set warning threshold',
      default=0
  )
  parser.add_argument(
      '-c', '--critical',
      type=int, help='set critical threshold',
      default=0
  )
  parser.add_argument(
    '-p', '--perfdata',
    action='store_true', help="Set perf data parsing"
  )
  args = parser.parse_args()


  if args.type == "example1":
    example1()
  elif args.type == "example2":
    example2()

  if len(perf) > 0:
    output = "{}|{}".format(output, perf)


  outputlines = output.count(',')
  output = output.replace(',', "\n")
  print_output(output,outputlines,countwarning,countcritical)
```